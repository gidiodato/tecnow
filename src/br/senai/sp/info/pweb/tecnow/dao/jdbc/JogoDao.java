package br.senai.sp.info.pweb.tecnow.dao.jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.senai.sp.info.pweb.tecnow.dao.DAO;
import br.senai.sp.info.pweb.tecnow.models.jogo.Categoria;
import br.senai.sp.info.pweb.tecnow.models.jogo.Jogo;
import br.senai.sp.info.pweb.tecnow.models.usuario.Usuario;

@Repository
public class JogoDao implements DAO<Jogo> {

	private FabricaDeConexoes conexao;

	public JogoDao() {

		conexao = new FabricaDeConexoes();

	}

	@Override
	public Jogo buscar(Long idJogo) {
		String sql = "SELECT idJogo, nomeJogo, categoria, dtCadastroJogo FROM jogo WHERE idJogo = ?";

		try {

			this.conexao.abrir();

			// Cria o stmt
			PreparedStatement statement = conexao.getConexao().prepareStatement(sql);

			// Passa o id do usu�rio para filtrar as categorias
			statement.setLong(1, idJogo);

			// Executa a busca
			ResultSet rs = statement.executeQuery();

			Jogo jogo = null;

			if (rs.next()) {

				jogo = new Jogo();
				jogo.setIdJogo(rs.getLong("idJogo"));
				jogo.setNomeJogo(rs.getString("nomeJogo"));
				Categoria categoria = Categoria.valueOf(rs.getString("categoria"));
				jogo.setCategoria(categoria);
				jogo.setDataCadastroJogo(rs.getDate("dtCadastroJogo"));

			}

			rs.close();

			return jogo;

		} catch (Exception e) {

			// Lan�a o erro para cima, fazendo a classe que chamae este m�todo tratar o erro
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Jogo> buscarTodos() {
		
		return null;
	}
	
	public List<Jogo> buscarTodos(Usuario usuario) {

		String sql = "SELECT idJogo, nomeJogo, categoria, dtCadastroJogo FROM jogo WHERE idUsuario = ?";

		List<Jogo> listaJogos = new ArrayList<>(100);

		try {

			this.conexao.abrir();

			// Cria o stmt
			PreparedStatement statement = conexao.getConexao().prepareStatement(sql);

			// Passa o id do usu�rio para filtrar as categorias
			statement.setLong(1, usuario.getIdUsuario());

			// Executa a busca
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {

				Jogo jogo = new Jogo();

				jogo.setIdJogo(rs.getLong("idJogo"));
				jogo.setNomeJogo(rs.getString("nomeJogo"));
				Categoria categoria = Categoria.valueOf(rs.getString("categoria"));
				jogo.setCategoria(categoria);
				jogo.setDataCadastroJogo(rs.getDate("dtCadastroJogo"));

				// Adiciona a categoria na lista
				listaJogos.add(jogo);

			}

			rs.close();

			return listaJogos;

		} catch (Exception e) {

			// Lan�a o erro para cima, fazendo a classe que chamae este m�todo tratar o erro
			throw new RuntimeException(e);

		}

		// Colocamos o fechar conex�o no finnaly pois ela deve ser encerrada tanto
		// se der erro 'catch' ou se der tudo certo :-)!

		finally {

			this.conexao.fechar();

		}
	}

	@Override
	public void alterar(Jogo obj) {
		String sql = "INSERT INTO jogo SET nomeJogo = ?, categoria = ?, dtCadastroJogo = ?, idUsuario = ?";

		try {

			conexao.abrir();

			PreparedStatement stmt = conexao.getConexao().prepareStatement(sql);

			stmt.setString(1, obj.getNomeJogo());
			stmt.setString(2, obj.getCategoria().toString());
			stmt.setDate(3, new Date(System.currentTimeMillis()));
			stmt.setLong(4, obj.getUsuario().getIdUsuario());

			stmt.execute();

		} catch (Exception e) {

			throw new RuntimeException(e);

		} finally {

			conexao.fechar();

		}

	}	

	

	@Override
	public void deletar(Jogo obj) {
		
		String sql = "DELETE FROM jogo WHERE idJogo = ?";

		try {

			conexao.abrir();

			PreparedStatement stmt = conexao.getConexao().prepareStatement(sql);

			// Aplica o id
			stmt.setLong(1, obj.getIdJogo());
			stmt.execute();

		} catch (Exception e) {

			throw new RuntimeException(e);

		} finally {

			conexao.fechar();

		}

	}

	@Override
	public void persistir(Jogo obj) {

		String sql = "INSERT INTO jogo SET nomeJogo = ?, categoria = ?, dtCadastroJogo = ?, idUsuario = ?";

		try {

			conexao.abrir();

			PreparedStatement stmt = conexao.getConexao().prepareStatement(sql);

			stmt.setString(1, obj.getNomeJogo());
			stmt.setString(2, obj.getCategoria().toString());
			stmt.setDate(3, new Date(System.currentTimeMillis()));
			stmt.setLong(4, obj.getUsuario().getIdUsuario());

			stmt.execute();

		} catch (Exception e) {

			throw new RuntimeException(e);

		} finally {

			conexao.fechar();

		}

	}

}
