package br.senai.sp.info.pweb.tecnow.dao.jdbc;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.senai.sp.info.pweb.tecnow.dao.DAO;
import br.senai.sp.info.pweb.tecnow.models.usuario.Sexos;
import br.senai.sp.info.pweb.tecnow.models.usuario.Usuario;

@Repository
public class UsuarioDao implements DAO<Usuario> {

	private FabricaDeConexoes conexao;

	public UsuarioDao() {

		conexao = new FabricaDeConexoes();

	}

	@Override
	public Usuario buscar(Long id) {

		return null;
	}

	@Override
	public List<Usuario> buscarTodos() {

		return null;
	}

	@Override
	public void alterar(Usuario obj) {

	}

	@Override
	public void deletar(Usuario obj) {

	}

	@Override
	public void persistir(Usuario obj) {

		String sql = "INSERT INTO usuario SET nomeUsuario = ?, email = ?, senha = ?, dtNascimento = ?, sexo = ?";

		try {

			conexao.abrir();

			PreparedStatement stmt = conexao.getConexao().prepareStatement(sql);

			stmt.setString(1, obj.getNomeUsuario());
			stmt.setString(2, obj.getEmail());
			stmt.setString(3, obj.getSenha());
			stmt.setDate(4, new Date(obj.getDataNascimento().getTime()));
			stmt.setString(5, obj.getSexo().toString());
			stmt.execute();

			conexao.fechar();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public Usuario autenticar(Usuario usuario) {

		String sql = "SELECT idUsuario, nomeUsuario, dtNascimento, sexo FROM usuario WHERE email = ? and senha  = ?";

		try {

			conexao.abrir();

			PreparedStatement stmt = conexao.getConexao().prepareStatement(sql);

			stmt.setString(1, usuario.getEmail());
			stmt.setString(2, usuario.getSenha());

			ResultSet resultados = stmt.executeQuery();
			Usuario usuarioAutenticado = null;

			if (resultados.next()) {

				usuarioAutenticado = new Usuario();
				usuarioAutenticado.setIdUsuario(resultados.getLong("idUsuario"));
				usuarioAutenticado.setDataNascimento(resultados.getDate("dtNascimento"));
				usuarioAutenticado.setNomeUsuario(resultados.getString("nomeUsuario"));
				usuarioAutenticado.setEmail(usuario.getEmail());
				usuarioAutenticado.setSenha(usuario.getSenha());
				// usuarioAutenticado.setSexo(resultados.getSexo().toString("sexo"));
				Sexos sexo = Sexos.valueOf(resultados.getString("sexo"));
				usuarioAutenticado.setSexo(sexo);

			}

			resultados.close();
			conexao.fechar();

			return usuarioAutenticado;

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return usuario;

	}

}
