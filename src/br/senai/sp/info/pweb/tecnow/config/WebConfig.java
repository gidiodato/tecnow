package br.senai.sp.info.pweb.tecnow.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import br.senai.sp.info.pweb.tecnow.interceptors.AutenticacaoPorSessaoInterceptor;

@Configuration
@ComponentScan("br.senai.sp.info.pweb.tecnow")
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

	// Ao chamar este m�todo o Spring gerenciar� as depend�ncias no objeto retornado
	@Bean
	public AutenticacaoPorSessaoInterceptor getInterceptor() {

		return new AutenticacaoPorSessaoInterceptor();

	}

	// Para Interceptors

	/**
	 * Este m�todo registra meus Interceptors no Spring
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		registry.addInterceptor((HandlerInterceptor) getInterceptor()).addPathPatterns("/**");

	}

	// Para abrir views
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {

		InternalResourceViewResolver resolver = new InternalResourceViewResolver();

		resolver.setViewClass(JstlView.class);
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");

		// Registrar a configura��o
		registry.viewResolver(resolver);

	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
	}

}
