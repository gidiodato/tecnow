package br.senai.sp.info.pweb.tecnow.models.usuario;

public enum Sexos {
	Feminino, Masculino
}
