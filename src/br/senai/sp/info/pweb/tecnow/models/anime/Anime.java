package br.senai.sp.info.pweb.tecnow.models.anime;

public class Anime {
	
	private Long idAnime;
	private String nomeAnime;
	
	@Override
	public String toString() {
		return "Anime [idAnime=" + idAnime + ", nomeAnime=" + nomeAnime + "]";
	}
	
	public Long getIdAnime() {
		return idAnime;
	}
	public void setIdAnime(Long idAnime) {
		this.idAnime = idAnime;
	}
	public String getNomeAnime() {
		return nomeAnime;
	}
	public void setNomeAnime(String nomeAnime) {
		this.nomeAnime = nomeAnime;
	}	
}
