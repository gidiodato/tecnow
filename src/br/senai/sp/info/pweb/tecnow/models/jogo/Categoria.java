package br.senai.sp.info.pweb.tecnow.models.jogo;

public enum Categoria {
	Tiro, RPG, Plataforma, 
	Esporte, Hack_and_Slash, Outro
}
