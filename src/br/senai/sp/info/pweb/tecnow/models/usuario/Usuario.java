package br.senai.sp.info.pweb.tecnow.models.usuario;


import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.DigestUtils;

public class Usuario {

	private Long idUsuario;
	private String nomeUsuario;
	private String email;
	private Sexos sexo;
	private String senha;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dataNascimento;

	// Criptografar Senha
	public void hashearSenha() {

		try {
			
			this.senha = DigestUtils.md5DigestAsHex(this.senha.getBytes("UTF-8"));

		} catch (Exception e) {

			throw new RuntimeException(e);

		}

	}
	
	//GGAS
	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Sexos getSexo() {
		return sexo;
	}

	public void setSexo(Sexos sexo) {
		this.sexo = sexo;
	}
	
	//Construtor
	public Usuario() {
		
	}
	
	//ToString
	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", nomeUsuario=" + nomeUsuario + ", email=" + email + ", senha="
				+ senha + ", dataNascimento=" + dataNascimento + ", sexo=" + sexo + "]";
	}
}
