package br.senai.sp.info.pweb.tecnow.models.jogo;

import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

import br.senai.sp.info.pweb.tecnow.models.usuario.Usuario;

public class Jogo {

	private Long idJogo;
	private String nomeJogo;
	private Categoria categoria;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dataCadastroJogo;
	private Usuario usuario;

	public Long getIdJogo() {
		return idJogo;
	}

	public void setIdJogo(Long idJogo) {
		this.idJogo = idJogo;
	}

	public String getNomeJogo() {
		return nomeJogo;
	}

	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Date getDataCadastroJogo() {
		return dataCadastroJogo;
	}

	public void setDataCadastroJogo(Date dataCadastroJogo) {
		this.dataCadastroJogo = dataCadastroJogo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Jogo() {

	}

	@Override
	public String toString() {
		return "Jogo [idJogo=" + idJogo + ", nomeJogo=" + nomeJogo + ", categoria=" + categoria + ", dataCadastroJogo="
				+ dataCadastroJogo + "]";
	}
}
