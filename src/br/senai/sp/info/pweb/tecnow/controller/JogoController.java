package br.senai.sp.info.pweb.tecnow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.senai.sp.info.pweb.tecnow.dao.jdbc.JogoDao;
import br.senai.sp.info.pweb.tecnow.models.jogo.Jogo;
import br.senai.sp.info.pweb.tecnow.utils.SessionUtils;

@Controller
@RequestMapping("/app/jogo")
public class JogoController {
	@Autowired
	private JogoDao jogoDAO;

	@Autowired
	private SessionUtils sessionUtils;

	@GetMapping("/deletar")
	public String deletar(@RequestParam(name = "idJogo", required = true) Long idJogo) {

		try {

			// Criar um objeto categoria, passar o id e deletar

			Jogo j = new Jogo();
			j.setIdJogo(idJogo);

			jogoDAO.deletar(j);

			return "redirect:/app";

		} catch (Exception e) {

		}

		return null;

	}
	
	@PostMapping("/salvar")
	public String salvar(Jogo jogo) {
		
		try {
			
			if(jogo.getIdJogo() == null) {
				
				jogo.setUsuario(sessionUtils.pegarUsuario());
				
				jogoDAO.persistir(jogo);
				
			} else {
				
				jogoDAO.alterar(jogo);
				
			}
			
		} catch( Exception e ) {
			
			throw new RuntimeException(e);
			
		}
		
		return "redirect:/app";
		
	}
			
}


