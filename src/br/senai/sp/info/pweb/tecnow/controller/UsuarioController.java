package br.senai.sp.info.pweb.tecnow.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import br.senai.sp.info.pweb.tecnow.dao.jdbc.UsuarioDao;
import br.senai.sp.info.pweb.tecnow.models.usuario.Usuario;
import br.senai.sp.info.pweb.tecnow.utils.SessionUtils;

@Controller
public class UsuarioController {
	// UsuarioDao usuarioDao = new UsuarioDao();
	@Autowired
	private UsuarioDao usuarioDao;

	@Autowired
	private SessionUtils sessionUtils;

	@GetMapping("/entrar")
	public String abreLogin() {

		return "Usuario/login";

	}

	@GetMapping("/sair")
	public String fazerLogout(HttpSession session) {

		sessionUtils.UnsetUsuarioLogado();

		return "index";

	}

	// Model envia dados para a View
	@PostMapping("/autenticar")
	public String autenticar(Usuario usuario, Model model) {

		List<String> erros = new ArrayList<>(6);

		if (usuario.getEmail() == null) {
			erros.add("O campo e-mail � obrigat�rio");
		} else if (usuario.getEmail().length() < 1 || usuario.getEmail().length() > 120) {
			erros.add("O campo e-mail deve conter entre 1 e 120 caracteres");
		}

		if (usuario.getSenha() == null) {
			erros.add("O campo senha � obrigat�rio");
		} else if (usuario.getSenha().length() < 2 || usuario.getSenha().length() > 20) {
			erros.add("O campo senha deve conter entre 2 e 20 caract�res");
		}

		// Verifica se a lista est� vazia (sem erros) ou n�o (com erros)
		// Se houver erros, retorne a p�gina com a(s) mensagem(s) de erro(s)
		if (!erros.isEmpty()) {

			model.addAttribute("erros", erros);
			return "Usuario/login";

		}

		// Autentica��o
		usuario.hashearSenha();

		Usuario usuarioAutenticado = usuarioDao.autenticar(usuario);

		if (usuarioAutenticado == null) {

			System.out.println("Usu�rio Inv�lido");

		}

		// Aplica o usu�rio na sess�o
		sessionUtils.setUsuarioLogado(usuarioAutenticado);

		System.out.println("Filtrado" + usuarioAutenticado);

		return "Cadastro/form";

	}

	@GetMapping("/usuario/novo")
	public String abreForm() {

		return "Usuario/form";

	}

	@PostMapping("/usuario/salvar")
	public String salvar(Usuario usuario, Model model) {

		List<String> erros = new ArrayList<>(6);

		// Valida��es
		//if(usuario.getEmail() == null && usuario.getSenha() == null && usuario.getNomeUsuario() == null) {
		//	erros.add("O campo e-mail, senha e nome s�o obrigat�rios");
		//}
		if (usuario.getEmail() == null) {
			erros.add("O campo e-mail � obrigat�rio");
		} else if (usuario.getEmail().length() < 1 || usuario.getEmail().length() > 120) {
			erros.add("O campo e-mail deve conter entre 1 e 120 caracteres");
		}

		if (usuario.getSenha() == null) {
			erros.add("O campo senha � obrigat�rio");
		} else if (usuario.getSenha().length() < 2 || usuario.getSenha().length() > 20) {
			erros.add("O campo senha deve conter entre 2 e 20 caract�res");
		}
		
		if (usuario.getNomeUsuario() == null) {
			erros.add("O campo de nome � obrigat�rio");
		} else if (usuario.getNomeUsuario().length() < 2 || usuario.getNomeUsuario().length() > 60) {
			erros.add("o campo nome deve conter entre 2 e 60 caracteres");
		}

		if (usuario.getDataNascimento().getTime() > System.currentTimeMillis()) {
			erros.add("O campo est� inv�lido");
		//}else if(usuario.getDataNascimento().getTime().equals() == null){
		//	
		}

		if (usuario.getSexo() == null) {
			erros.add("O campo sexo � obrigat�rio");
		}

		// Verifica se a lista est� vazia (sem erros) ou n�o (com erros)
		// Se houver erros, retorne a p�gina com a(s) mensagem(s) de erro(s)
		if (!erros.isEmpty()) {

			model.addAttribute("erros", erros);
			return "Usuario/form";

		}

		usuario.hashearSenha();
		usuarioDao.persistir(usuario);

		System.out.println(usuario);

		return "index";

	}

	}

