package br.senai.sp.info.pweb.tecnow.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.senai.sp.info.pweb.tecnow.dao.jdbc.JogoDao;
import br.senai.sp.info.pweb.tecnow.models.jogo.Jogo;
import br.senai.sp.info.pweb.tecnow.models.usuario.Usuario;

@Controller
@RequestMapping(value = "/app")
public class CadastroJogoController {
	@Autowired
	private JogoDao jogoDAO;

	@GetMapping("/cadastro")
	public String abrirForm(@RequestParam(name = "idJogo", required = false) Long idJogo, Model model) {

		// Se o id for informado, � uma altera��o.
		// Logo, buscaremos no banco de dados para envi�-lo para tela
		if (idJogo != null) {

			Jogo jogo = jogoDAO.buscar(idJogo);
			model.addAttribute("jogo", jogo);

		}

		return "Cadastro/form";

	}

	@GetMapping(value = { "/", "" }) // Aceita terminar com "/" ou sem
	public String abrePaginaDeCadastros(Model model, HttpSession session) {

		// Busca o usu�rio logado na sess�o
		Usuario logado = (Usuario) session.getAttribute("usuarioLogado");

		// Passo o usu�rio logado para buscar suas categorias
		model.addAttribute("jogos", jogoDAO.buscarTodos(logado));

		return "Cadastro/lista";

	}

}
