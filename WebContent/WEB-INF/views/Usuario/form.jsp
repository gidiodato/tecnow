<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cadastro de Usuários</title>
</head>
<body>

	<form method="post" action="/tecnow/usuario/salvar">
	
		<label>
			Nome:
			<input type="text" name="nomeUsuario" maxlength="60">
		
		</label><br/>
		
		<label>
			Data de nascimento:
			<input type="date" name="dataNascimento">
		
		</label><br/>
		
		<c:if test="${usuario.sexo eq Sexos.Feminino }">
		<input type="radio" name="sexo" value="Feminino" checked> Feminino<br>
		</c:if>
		
		<c:if test="${usuario.sexo eq Sexos.Masculino }">
 		<input type="radio" name="sexo" value="Masculino" checked> Masculino<br>
 		</c:if>
		
		<label>
			E-mail:
			<input type="email" name="email">
		
		</label><br/>
		
		<label>
			Senha:
			<input type="password" name="senha">
		
		</label><br/>
		
		<button type="submit">Salvar</button>
	
	</form>

</body>
</html>