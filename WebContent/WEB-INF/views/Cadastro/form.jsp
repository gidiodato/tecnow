<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="../Templates/head.jsp"></c:import>
<body>

	<c:import url="../Templates/cabecalho.jsp"></c:import>

	<h1>Cadastro de Jogos</h1>
	
	<c:url value="/app/jogo/salvar" var="salvarJogo"></c:url>
	<form action="${salvarJogo}" method="post">
	
	<%-- Campo escondido para guardar o id --%>
	<input type="hidden" value="${jogo.idJogo}" name="id">
	
		<label>
			Nome do Jogo:
			<input type="text" name="nomeJogo" maxlength="40" value="${jogo.nomeJogo}">
		</label><br>
		
		<c:if test="${jogo.categoria eq Categoria.Tiro }">
		<input type="radio" name="categoria" value="Tiro" checked> Tiro<br>
		</c:if>
		
		<c:if test="${jogos.categoria eq Categoria.RPG }">
 		<input type="radio" name="categoria" value="RPG" checked> RPG<br>
 		</c:if>
		
		<c:if test="${jogos.categoria eq Categoria.Plataforma }">
 		<input type="radio" name="categoria" value="Plataforma" checked> Plataforma<br>
 		</c:if>
 		
 		<c:if test="${jogos.categoria eq Categoria.Masculino }">
 		<input type="radio" name="categoria" value="Esporte" checked> Esporte<br>
 		</c:if>
 		
 		<c:if test="${jogos.categoria eq Categoria.Hack_and_Slash }">
 		<input type="radio" name="categoria" value="Hack_and_Slash" checked> Hack_and_Slash<br>
 		</c:if>
 		
 		<c:if test="${jogos.categoria eq Categoria.Outro }">
 		<input type="radio" name="categoria" value="Outro" checked> Outro<br>
 		</c:if>
		
		
		<button type="submit"> Salvar </button>
	
	</form>

</body>
</html>