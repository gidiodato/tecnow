<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/app/jogo/deletar" var="urlDeletarJogo"></c:url>
<c:url value="/app/cadastro" var="urlTelaCadastro"></c:url>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista de Jogos</title>
</head>
<body>

	<c:import url="../Templates/cabecalho.jsp"></c:import>

	<h1>Autenticado</h1>
	
	<h2>Lista de jogos</h2>
	
	<%-- Desenhando a tabela para listar a categorias --%>
	<table>
	
		<thead>
		
			<tr>
			
				<th>Nome</th>
				<th>Categoria</th>
				<th>Data de Cadastro</th>
				<th>Ações</th>
				
			</tr>
			
		</thead>
		
		<tbody>
		
			<%-- Gerar o conteúdo para listar as categorias --%>
			
			<c:forEach items="${jogos}" var="jogo">
			
				<tr>
				
					<td>${jogo.nomeJogo}</td>
					<td>${jogo.categoria}</td>
					<td>${jogo.dataCadastroJogo}</td>
					<td><a href="${urlDeletarJogo}?id=${jogo.idJogo}">Deletar</a></td>
					<td><a href="${urlTelaCadastro}?id=${jogo.idJogo}">Alterar</a></td>
					
				</tr>
			
			</c:forEach>
			
		</tbody>
	
	</table>
	
</body>
</html>